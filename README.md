# KDE Translation How-To

Draft of the manual for KDE translators.

Current goals:

* The translation howto should be task-oriented
* It should be concise.
* It should address the following topics
  * “I want to start translating language foo”
  * “I’ve tried contacting language team foo but no answer”
     when a team exist, when a team doesn’t exist (or doesn’t exist anymore)
  * “I’ve got translation access, how do I move forward”
    clear information on where translations can be found and the meaning of branches, and how to update and commit.
  * Advanced information are fine, but should not hinder the comprehension of the the simple tasks

## Build

```
meinproc5 ../translation-howto.docbook
```

## License

This project is licensed under FDL-1.3-or-later.
